import re
import csv

THREAD_PATTERN = "(\".*\") #(\d{0,100}) prio=(\d{0,100}) os_prio=(\d{0,100}) cpu=(.*)ms elapsed=(.*)s tid=(.*) nid=(.{6,8}) (.*)  (\[.*\])"
DEMON_THREAD_PATTERN = "(\".*\") #(\d{0,100}) (daemon) prio=(\d{0,100}) os_prio=(\d{0,100}) cpu=(.*)ms elapsed=(.*)s tid=(.*) nid=(.{6,8}) (.*)  (\[.*\])"
fields = ['thread_name', 'thread_id', 'thread_prio', 'thread_os_prio','thread_cpu', 'thread_elapsed_time' ,'thread_tid' , 'thread_nid' ,'thread_state' , 'thread_mem_adr',"is_thread_demon"] 

def export(file_path, matrix):
    global fields
    with open(file_path,"w") as export_file:
        write = csv.writer(export_file)
        write.writerow(fields)
        write.writerows(matrix)

def parse_line(line):
    match_non_demon = re.search(THREAD_PATTERN, line)
    match_demon = re.search(DEMON_THREAD_PATTERN, line)
    _list = []
    thread_demon = False
    if (match_non_demon):
        match = match_non_demon
        thread_name = match.group(1)
        thread_id = match.group(2)
        thread_prio = match.group(3)
        thread_os_prio = match.group(4)
        thread_cpu = match.group(5)
        thread_elapsed_time = match.group(6)
        thread_tid = match.group(7)
        thread_nid = match.group(8)
        thread_state = match.group(9)
        thread_mem_adr = match.group(10)
        _list.extend([thread_name,thread_id,thread_prio,thread_os_prio,thread_cpu, thread_elapsed_time,thread_tid,thread_nid,thread_state,thread_mem_adr,thread_demon]) 
    elif(match_demon):
        match = match_demon
        thread_name = match.group(1)
        thread_id = match.group(2)
        thread_demon = True
        thread_prio = match.group(4)
        thread_os_prio = match.group(5)
        thread_cpu = match.group(6)
        thread_elapsed_time = match.group(7)
        thread_tid = match.group(8)
        thread_nid = match.group(9)
        thread_state = match.group(10)
        thread_mem_adr = match.group(11)
        _list.extend([thread_name,thread_id,thread_prio,thread_os_prio,thread_cpu, thread_elapsed_time,thread_tid,thread_nid,thread_state,thread_mem_adr,thread_demon]) 
    
    return _list

def analysis(file_path):
    # for line in file :
    #     print(line)
    
    all_thread = []
    with open(file_path) as file:
        for line in file:
            current_thread = parse_line(line)
            if len (current_thread) > 9:
                all_thread.append(current_thread)
    print(all_thread)
    export( file_path + ".csv", all_thread)
    

def main():
    file_path = "thread_dump.txt"
    analysis(file_path)
if __name__ == "__main__":
    main()